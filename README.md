## Track Your Habits
An application created for tracking our habits. 
Using this application we are able to track our habits. We can create a profile, add habits we want
to track and manage the related things. We can look back and see whether we really practice our
habits or not.

It was built with Spring Boot and Hibernate. The application uses PostgreSQL database.

Authentication is done with JSON Web Token which is stored in HTTP Only Cookie.

Testing was made by JUnit5 and Mockito.

Maven is responsible for the build automation system.

As a remote version control repository BitBucket was used because it can be easily integrated with Jira.

The application was developed using the Gitflow Workflow.


## Project status
* Implemented features:
	* registration
	* sending confirmation email after registration
	* confirm account
	* login
	* listing habits by user
	* adding new habit
	* updating habit (name, general description, description of regularity)
	* deleting habit

* Not yet implemented features:
	* setting progress day by day
	* CRUD operations regarding excuses
	* stop tracking a habit
	* logout
	
## Prerequisites
* Java 14 
* Maven 3.6.0
* Create a PostgreSQL database and set up the following enviroment variables:
	* TRACK_YOUR_HABITS_POSTGRESQL_DB_PORT
	* TRACK_YOUR_HABITS_POSTGRESQL_DB_NAME
	* TRACK_YOUR_HABITS_POSTGRESQL_DB_USERNAME
	* TRACK_YOUR_HABITS_POSTGRESQL_DB_PASSWORD


## You can take a look at the API documentation after running the application:
http://localhost:8080/swagger-ui.html

If you want to add new habit then the timesOfDay can only be MORNING, MIDDAY, AFTERNOON or EVENING. For now the list can have only one element.

When you update a habit you can only modify the name, the general description, the description of regularity fields. 


## Reflection
* The main project goals were: 
	* to make my code self-documented
	* to comply with SOLID principles
	* to write unit tests