package mate.rajnai.trackyourhabits.controller;

import lombok.RequiredArgsConstructor;
import mate.rajnai.trackyourhabits.model.dto.LoginDetailsDto;
import mate.rajnai.trackyourhabits.model.dto.AuthResponseDto;
import mate.rajnai.trackyourhabits.model.enumerate.CookieType;
import mate.rajnai.trackyourhabits.security.CookieService;
import mate.rajnai.trackyourhabits.security.jwttoken.JwtTokenService;
import mate.rajnai.trackyourhabits.service.LoginService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/login")
@RequiredArgsConstructor
public class LoginController {

    private static final String MESSAGE_WHEN_LOGIN_IS_FAILED_BECAUSE_OF_WRONG_CREDENTIALS = "Invalid username/password supplied!";

    private final LoginService loginService;
    private final CookieService cookieService;
    private final JwtTokenService jwtTokenService;

    @PostMapping
    public ResponseEntity<AuthResponseDto> login(@Valid @RequestBody LoginDetailsDto loginDetails,
                                                 HttpServletResponse response) {
        try {
            AuthResponseDto authResponseDto = this.loginService.login(loginDetails);
            if (authResponseDto.isSuccess()) {
                String token = this.getCreatedToken(authResponseDto);
                response.addCookie(this.getCreatedCookieContainingToken(token));
            }
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(authResponseDto);

        } catch (AuthenticationException e) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body(this.loginService
                            .createResponseAfterFailedLogin(MESSAGE_WHEN_LOGIN_IS_FAILED_BECAUSE_OF_WRONG_CREDENTIALS));
        }
    }

    private String getCreatedToken(AuthResponseDto authResponseDto) {
        return this.jwtTokenService
                .createToken(authResponseDto.getData().get("id").toString(),
                        authResponseDto.getData().get("username").toString(),
                        (List<String>) authResponseDto.getData().get("roles"));
    }

    private Cookie getCreatedCookieContainingToken(String token) {
        return this.cookieService
                .createCookie(CookieType.JWT_HTTP_ONLY_NOT_SECURE.getName(),
                        token,
                        CookieType.JWT_HTTP_ONLY_NOT_SECURE.isHttpOnly(),
                        CookieType.JWT_HTTP_ONLY_NOT_SECURE.isSecure(),
                        CookieType.JWT_HTTP_ONLY_NOT_SECURE.getMaxAge(),
                        CookieType.JWT_HTTP_ONLY_NOT_SECURE.getPath());
    }
}
