package mate.rajnai.trackyourhabits.controller;

import lombok.RequiredArgsConstructor;
import mate.rajnai.trackyourhabits.exception.HabitNotFoundException;
import mate.rajnai.trackyourhabits.model.Habit;
import mate.rajnai.trackyourhabits.model.dto.HabitDetailsDto;
import mate.rajnai.trackyourhabits.service.HabitService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/habits")
@RequiredArgsConstructor
public class HabitController {

    private final HabitService habitService;
    
    @GetMapping
    public ResponseEntity<List<Habit>> listHabitsByUser() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(this.habitService.getHabitsByUser());
    }

    @PostMapping
    public ResponseEntity<Habit> addNewHabitToUser(@Valid @RequestBody HabitDetailsDto habitDetailsDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(this.habitService.addNewHabitToUser(habitDetailsDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Habit> updateHabit(@PathVariable(value = "id") Long habitId, @Valid @RequestBody HabitDetailsDto habitDetailsDto) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(this.habitService.updateHabit(habitDetailsDto, habitId));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteHabit(@PathVariable(value = "id") Long habitId) {
        this.habitService.deleteHabit(habitId);
        return ResponseEntity
                .status(HttpStatus.OK)
                .build();
    }
}
