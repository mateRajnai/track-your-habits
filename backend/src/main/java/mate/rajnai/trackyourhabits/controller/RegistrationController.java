package mate.rajnai.trackyourhabits.controller;

import lombok.RequiredArgsConstructor;
import mate.rajnai.trackyourhabits.model.dto.AuthResponseDto;
import mate.rajnai.trackyourhabits.model.dto.RegistrationDetailsDto;
import mate.rajnai.trackyourhabits.service.RegistrationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/registration")
@RequiredArgsConstructor
public class RegistrationController {

    private final RegistrationService registrationService;

    @PostMapping
    public ResponseEntity<AuthResponseDto> register(@Valid @RequestBody RegistrationDetailsDto registrationDetailsDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(registrationService.register(registrationDetailsDto));
    }

    @GetMapping("/confirm-account")
    public ResponseEntity<AuthResponseDto> confirmAccount(@RequestParam("token") String confirmationToken) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(registrationService.confirmAccount(confirmationToken));
    }
}
