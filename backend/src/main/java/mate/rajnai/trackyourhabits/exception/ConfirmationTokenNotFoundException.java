package mate.rajnai.trackyourhabits.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "The link is invalid or broken")
public class ConfirmationTokenNotFoundException extends NullPointerException {
    public ConfirmationTokenNotFoundException(String messageWhenEmailConfirmationFailed) {
        super(messageWhenEmailConfirmationFailed);
    }
}
