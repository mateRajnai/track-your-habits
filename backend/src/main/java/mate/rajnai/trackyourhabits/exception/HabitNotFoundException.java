package mate.rajnai.trackyourhabits.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "User has no such habit")
public class HabitNotFoundException extends NullPointerException {

    public HabitNotFoundException(String messageWhenHabitNotFound) {
        super(messageWhenHabitNotFound);
    }
}
