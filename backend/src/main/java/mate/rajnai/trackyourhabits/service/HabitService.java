package mate.rajnai.trackyourhabits.service;

import lombok.RequiredArgsConstructor;
import mate.rajnai.trackyourhabits.exception.HabitNotFoundException;
import mate.rajnai.trackyourhabits.exception.UserNotFoundException;
import mate.rajnai.trackyourhabits.model.AppUser;
import mate.rajnai.trackyourhabits.model.Habit;
import mate.rajnai.trackyourhabits.model.enumerate.TimeOfDay;
import mate.rajnai.trackyourhabits.model.dto.HabitDetailsDto;
import mate.rajnai.trackyourhabits.repository.AppUserRepository;
import mate.rajnai.trackyourhabits.repository.HabitRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class HabitService {

    private static final String MESSAGE_WHEN_USER_NOT_FOUND = "User not found";
    private static final String MESSAGE_WHEN_HABIT_NOT_FOUND = "User has no such habit";

    private final AppUserRepository users;
    private final HabitRepository habits;
    private final UserInfoService userInfoService;

    public Habit addNewHabitToUser(HabitDetailsDto habitDetailsDto) {
        AppUser appUser = this.getCurrentUser();
        Set<TimeOfDay> timesOfDay = this.convertTimesOfDayInputToEnum(habitDetailsDto);
        Habit habit = this.createHabit(habitDetailsDto, appUser, timesOfDay);
        return habits.saveAndFlush(habit);
    }

    private AppUser getCurrentUser() {
        String nameOfCurrentUser = userInfoService.getNameOfCurrentUser();
        return this.users.findByUsername(nameOfCurrentUser)
                .orElseThrow(() -> new UserNotFoundException(MESSAGE_WHEN_USER_NOT_FOUND));
    }

    private Set<TimeOfDay> convertTimesOfDayInputToEnum(HabitDetailsDto habitDetailsDto) {
        List<String> timesOfDayInput = habitDetailsDto.getTimesOfDay();
        return timesOfDayInput.stream().map(TimeOfDay::valueOf).collect(Collectors.toSet());
    }

    private Habit createHabit(HabitDetailsDto habitDetailsDto, AppUser appUser, Set<TimeOfDay> timesOfDay) {
        return Habit.builder()
                .name(habitDetailsDto.getName())
                .generalDescription(habitDetailsDto.getGeneralDescription())
                .goal(habitDetailsDto.getGoal())
                .descriptionOfRegularity(habitDetailsDto.getDescriptionOfRegularity())
                .timesOfDay(timesOfDay)
                .reward(habitDetailsDto.getReward())
                .startingDate(LocalDateTime.now())
                .user(appUser)
                .build();
    }


    public List<Habit> getHabitsByUser() {
        AppUser user = this.getCurrentUser();
        return this.habits.findByUser(user);
    }


    public Habit updateHabit(HabitDetailsDto habitDetailsDto, Long habitId) {
        AppUser user = this.getCurrentUser();
        Habit habit = this.habits.findByIdAndUser(habitId, user)
                .orElseThrow(() -> new HabitNotFoundException(MESSAGE_WHEN_HABIT_NOT_FOUND));
        this.updateModifiableFields(habit, habitDetailsDto);
        return this.habits.saveAndFlush(habit);
    }

    private void updateModifiableFields(Habit habit, HabitDetailsDto habitDetailsDto) {
        habit.updateNameIfInputFieldNotNull(habitDetailsDto.getName());
        habit.updateGeneralDescriptionIfInputFieldNotNull(habitDetailsDto.getGeneralDescription());
        habit.updateDescriptionOfRegularityIfInputFieldNotNull(habitDetailsDto.getDescriptionOfRegularity());
    }


    public void deleteHabit(Long habitId) {
        AppUser user = this.getCurrentUser();
        this.habits.deleteByIdAndUser(habitId, user);
    }
}
