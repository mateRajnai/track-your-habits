package mate.rajnai.trackyourhabits.service;

import lombok.RequiredArgsConstructor;
import mate.rajnai.trackyourhabits.exception.ConfirmationTokenNotFoundException;
import mate.rajnai.trackyourhabits.model.ConfirmationTokenOfRegistration;
import mate.rajnai.trackyourhabits.model.enumerate.EmailType;
import mate.rajnai.trackyourhabits.model.dto.AuthResponseDto;
import mate.rajnai.trackyourhabits.model.AppUser;
import mate.rajnai.trackyourhabits.model.enumerate.Role;
import mate.rajnai.trackyourhabits.model.dto.RegistrationDetailsDto;
import mate.rajnai.trackyourhabits.repository.AppUserRepository;
import mate.rajnai.trackyourhabits.repository.ConfirmationTokenOfRegistrationRepository;
import org.springframework.mail.MailException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class RegistrationService {

    private static final String MESSAGE_WHEN_REGISTRATION_IS_SUCCESSFUL = "Registration is successful! An email is sent to you. To log in, please verify your email!";
    private static final String MESSAGE_WHEN_REGISTRATION_IS_FAILED = "Username and/or email is occupied";
    private static final String MESSAGE_WHEN_EMAIL_CONFIRMATION_FAILED = "The link is invalid or broken";
    private static final String MESSAGE_WHEN_EMAIL_CONFIRMATION_SUCCESSFUL = "Your email address is confirmed. Now you can log in.";
    private static final String URL_PATH_OF_CONFIRMATION_TOKEN = "http://localhost:8080/api/registration/confirm-account?token=";

    private final AppUserRepository users;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;
    private final ConfirmationTokenOfRegistrationRepository confirmationTokenRepository;

    public AuthResponseDto register(RegistrationDetailsDto registrationDetailsDto) throws MailException {
        if (this.isRegistrationFailed(registrationDetailsDto))
            return this.createResponseAfterFailedRegistration(registrationDetailsDto);

        AppUser user = this.buildNewAppUser(registrationDetailsDto);
        users.saveAndFlush(user);

        ConfirmationTokenOfRegistration confirmationToken = ConfirmationTokenOfRegistration
                .builder()
                .user(user)
                .build();
        confirmationTokenRepository.saveAndFlush(confirmationToken);

        emailService.sendEmail(registrationDetailsDto.getEmail(),
                EmailType.AFTER_REGISTRATION.getSubject(),
                EmailType.AFTER_REGISTRATION.getText().concat(this.createConfirmationLink(confirmationToken)));
        return this.createResponseAfterSuccessfulRegistration();
    }

    private AuthResponseDto createResponseAfterSuccessfulRegistration() {
        return AuthResponseDto
                .builder()
                .success(true)
                .message(MESSAGE_WHEN_REGISTRATION_IS_SUCCESSFUL)
                .build();
    }

    private AuthResponseDto createResponseAfterFailedRegistration(RegistrationDetailsDto registrationDetailsDto) {
        boolean usernameExists = users.existsByUsername(registrationDetailsDto.getUsername());
        boolean emailExists = users.existsByEmail(registrationDetailsDto.getEmail());
        AuthResponseDto response = AuthResponseDto.builder()
                .success(false)
                .message(MESSAGE_WHEN_REGISTRATION_IS_FAILED).build();
        response.addData("usernameIsOccupied", usernameExists);
        response.addData("emailIsOccupied", emailExists);
        return response;
    }

    private boolean isRegistrationFailed(RegistrationDetailsDto registrationDetailsDto) {
        boolean usernameExists = users.existsByUsername(registrationDetailsDto.getUsername());
        boolean emailExists = users.existsByEmail(registrationDetailsDto.getEmail());
        return usernameExists || emailExists;
    }

    private AppUser buildNewAppUser(RegistrationDetailsDto registrationDetailsDto) {
        AppUser user = AppUser.builder()
                .username(registrationDetailsDto.getUsername())
                .firstName(registrationDetailsDto.getFirstName())
                .email(registrationDetailsDto.getEmail())
                .password(passwordEncoder.encode(registrationDetailsDto.getPassword()))
                .registrationDate(LocalDateTime.now())
                .roles(new ArrayList<>())
                .isEnabled(false)
                .build();
        user.addRole(Role.USER);
        return user;
    }

    private String createConfirmationLink(ConfirmationTokenOfRegistration confirmationToken) {
        return URL_PATH_OF_CONFIRMATION_TOKEN.concat(confirmationToken.getConfirmationToken());
    }


    public AuthResponseDto confirmAccount(String confirmationToken) throws ConfirmationTokenNotFoundException {
        ConfirmationTokenOfRegistration confirmationTokenOfRegistration = confirmationTokenRepository
                .findByConfirmationToken(confirmationToken)
                .orElseThrow(() -> new ConfirmationTokenNotFoundException(MESSAGE_WHEN_EMAIL_CONFIRMATION_FAILED));
        AppUser user = confirmationTokenOfRegistration.getUser();
        user.setEnabled(true);
        users.saveAndFlush(user);
        confirmationTokenRepository.delete(confirmationTokenOfRegistration);
        return AuthResponseDto.builder()
                .success(true)
                .message(MESSAGE_WHEN_EMAIL_CONFIRMATION_SUCCESSFUL)
                .build();
    }
}
