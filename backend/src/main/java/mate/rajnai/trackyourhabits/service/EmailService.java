package mate.rajnai.trackyourhabits.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EmailService {

    @Value("${spring.mail.username}")
    private static String EMAIL_FROM;

    private SimpleMailMessage mailMessage;
    private final JavaMailSender javaMailSender;


    public void sendEmail(String to, String subject, String content) throws MailException {
        mailMessage = createEmailMessage(to, subject, content);
        javaMailSender.send(mailMessage);
    }

    private SimpleMailMessage createEmailMessage(String to, String subject, String content) {
        mailMessage.setTo(to);
        mailMessage.setFrom(EMAIL_FROM);
        mailMessage.setSubject(subject);
        mailMessage.setText(content);
        return mailMessage;
    }
}
