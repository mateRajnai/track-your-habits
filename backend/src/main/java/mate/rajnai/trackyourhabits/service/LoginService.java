package mate.rajnai.trackyourhabits.service;

import lombok.RequiredArgsConstructor;
import mate.rajnai.trackyourhabits.exception.UserNotFoundException;
import mate.rajnai.trackyourhabits.model.AppUser;
import mate.rajnai.trackyourhabits.model.dto.LoginDetailsDto;
import mate.rajnai.trackyourhabits.model.dto.AuthResponseDto;
import mate.rajnai.trackyourhabits.repository.AppUserRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LoginService {

    private static final String MESSAGE_WHEN_LOGIN_IS_SUCCESSFUL = "Login is successful!";
    private static final String MESSAGE_WHEN_LOGIN_IS_FAILED_BECAUSE_OF_NOT_CONFIRMED_REGISTRATION = "Please confirm your registration in the email sent by us! After it you can log in.";
    private static final String MESSAGE_WHEN_USER_NOT_FOUND = "User not found";

    private final AuthenticationManager authenticationManager;
    private final AppUserRepository users;

    public AuthResponseDto login(LoginDetailsDto loginDetails) {
        String username = loginDetails.getUsername();
        AppUser user = users.findByUsername(username).orElseThrow(() -> new UserNotFoundException(MESSAGE_WHEN_USER_NOT_FOUND));
        if (user.isEnabled()) {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, loginDetails.getPassword()));
            List<String> roles = authentication.getAuthorities()
                    .stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList());
            String userId = user.getId().toString();
            return this.createResponseAfterSuccessfulLogin(userId, username, roles);
        }
        return this.createResponseAfterFailedLogin(MESSAGE_WHEN_LOGIN_IS_FAILED_BECAUSE_OF_NOT_CONFIRMED_REGISTRATION);
    }

    private AuthResponseDto createResponseAfterSuccessfulLogin(String userId, String username, List<String> roles) {
        AuthResponseDto authResponseDto = AuthResponseDto
                .builder()
                .success(true)
                .message(MESSAGE_WHEN_LOGIN_IS_SUCCESSFUL)
                .build();
        authResponseDto.addData("id", userId);
        authResponseDto.addData("username", username);
        authResponseDto.addData("roles", roles);
        return authResponseDto;
    }

    public AuthResponseDto createResponseAfterFailedLogin(String reasonOfFailedLogin) {
        return AuthResponseDto
                .builder()
                .success(false)
                .message(reasonOfFailedLogin)
                .build();
    }
}
