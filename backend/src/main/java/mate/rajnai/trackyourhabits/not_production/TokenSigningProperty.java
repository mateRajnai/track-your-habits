package mate.rajnai.trackyourhabits.not_production;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@ConfigurationProperties("trackyourhabits.token.signing.key")
public final class TokenSigningProperty {

    /**
     * Secret key for signing token.
     * */
    private final String secretKey = "sasftfthbfchscefotu7z";

}
