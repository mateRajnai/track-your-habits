package mate.rajnai.trackyourhabits.repository;

import mate.rajnai.trackyourhabits.model.AppUser;
import mate.rajnai.trackyourhabits.model.ConfirmationTokenOfRegistration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ConfirmationTokenOfRegistrationRepository extends JpaRepository<ConfirmationTokenOfRegistration, Long> {
    Optional<ConfirmationTokenOfRegistration> findByConfirmationToken(String confirmationToken);

    ConfirmationTokenOfRegistration findByUser(AppUser appUser);
}
