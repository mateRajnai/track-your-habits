package mate.rajnai.trackyourhabits.repository;

import mate.rajnai.trackyourhabits.model.AppUser;
import mate.rajnai.trackyourhabits.model.Habit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface HabitRepository extends JpaRepository<Habit, Long> {
    List<Habit> findByUser(AppUser user);

    Optional<Habit> findByIdAndUser(Long id, AppUser user);

    @Transactional
    void deleteByIdAndUser(Long id, AppUser user);
}
