package mate.rajnai.trackyourhabits.security.jwttoken;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Getter
@Component
@NoArgsConstructor
public class JwtTokenBlackListDaoMem {
    private final List<String> jwtTokenBlackList = new LinkedList<>();

    public void addTokenToBlackList(String token){
        this.jwtTokenBlackList.add(token);
    }
}
