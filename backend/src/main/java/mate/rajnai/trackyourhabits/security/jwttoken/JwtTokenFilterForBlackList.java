package mate.rajnai.trackyourhabits.security.jwttoken;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RequiredArgsConstructor
public class JwtTokenFilterForBlackList extends GenericFilterBean {

    private final JwtTokenBlackListDaoMem blackList;
    private final JwtTokenService jwtTokenService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        String token = jwtTokenService.getTokenFromRequest((HttpServletRequest) request);
        if(token != null && blackList.getJwtTokenBlackList().contains(token)) {
            throw new AuthorizationServiceException("Invalid token (on blacklist)");
        }
        filterChain.doFilter(request, response);
    }
}
