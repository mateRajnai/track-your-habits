package mate.rajnai.trackyourhabits.security.jwttoken;

import lombok.RequiredArgsConstructor;
import mate.rajnai.trackyourhabits.security.CookieService;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
public class JwtTokenDoBlackListing implements LogoutHandler {

    private final JwtTokenBlackListDaoMem blackList;
    private final JwtTokenService jwtTokenService;
    private final CookieService cookieService;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String token = jwtTokenService.getTokenFromRequest(request);
        if (token != null) {
            blackList.addTokenToBlackList(token);
        }
        response.addCookie(cookieService.invalidateCookie(request, "jwt", true));
    }
}
