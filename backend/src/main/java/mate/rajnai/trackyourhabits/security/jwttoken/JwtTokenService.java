package mate.rajnai.trackyourhabits.security.jwttoken;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import mate.rajnai.trackyourhabits.not_production.TokenSigningProperty;
import mate.rajnai.trackyourhabits.model.enumerate.Role;
import mate.rajnai.trackyourhabits.security.RoleEnumGrantedAuthority;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.*;


@Slf4j
@Service
public class JwtTokenService {

    public JwtTokenService(TokenSigningProperty tokenSigningProperty) {
        this.secretKey = tokenSigningProperty.getSecretKey();
    }

    private String secretKey;

    @Value("${security.jwt.token.expire-length:3600000}")
    private long validityInMilliseconds = 36000000; // 10 hours

    private final String userIdFieldName = "id";

    private final String rolesFieldName = "roles";


    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createToken(String userId, String username, List<String> roles) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put(userIdFieldName, userId);
        claims.put(rolesFieldName, roles);

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    String getTokenFromRequest(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            return Arrays.stream(cookies)
                    .filter(cookie -> "jwt".equals(cookie.getName()))
                    .findFirst()
                    .map(Cookie::getValue)
                    .orElse(null);
        }
        return null;
    }

    boolean validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            if (claims.getBody().getExpiration().before(new Date())) {
                return false;
            }
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            log.debug("JWT token invalid " + e);
        }
        return false;
    }

    /**
     * Parses the username and roles from the token. Since the token is signed we can be sure its valid information.
     * Note that it does not make a DB call to be super fast!
     * This could result in returning false data (e.g. the user was deleted, but their token has not expired yet)
     * To prevent errors because of this make sure to check the user in the database for more important calls!
     */
    Authentication parseUserFromTokenInfo(String token) throws UsernameNotFoundException {
        Claims body = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
        String username = body.getSubject();
        List<String> roles = (List<String>) body.get(rolesFieldName);
        List<RoleEnumGrantedAuthority> authorities = new LinkedList<>();
        for (String role : roles) {
            authorities.add(new RoleEnumGrantedAuthority(Role.valueOf(role)));
        }

        return new UsernamePasswordAuthenticationToken(username, "", authorities);
    }
}
