package mate.rajnai.trackyourhabits.security;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Service
@NoArgsConstructor
public class CookieService {

    public Cookie createCookie(String name, String value, boolean isHttpOnly, boolean isSecure, int maxAge, String path) {
        Cookie cookie = new Cookie(name, value);
        cookie.setHttpOnly(isHttpOnly);
        cookie.setSecure(isSecure);
        cookie.setMaxAge(maxAge);
        cookie.setPath(path);
        return cookie;
    }

    public Cookie invalidateCookie(HttpServletRequest request, String cookieName, boolean isHttpOnly) {
        Cookie[] cookies = request.getCookies();
        Cookie cookieToBeInvalidated = null;
        if (cookies != null) {
            cookieToBeInvalidated = Arrays.stream(cookies)
                    .filter((cookie) -> cookieName.equals(cookie.getName()))
                    .findFirst()
                    .map((cookie) -> {
                        cookie.setValue(null);
                        cookie.setHttpOnly(isHttpOnly);
                        cookie.setMaxAge(0);
                        return cookie;
                    })
                    .orElse(null);
        }
        return cookieToBeInvalidated;
    }
}
