package mate.rajnai.trackyourhabits.security;

import lombok.RequiredArgsConstructor;
import mate.rajnai.trackyourhabits.security.jwttoken.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final String[] SWAGGER_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    private final CookieService cookieService;
    private final JwtTokenService jwtTokenService;
    private final JwtTokenBlackListDaoMem blackList;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(SWAGGER_WHITELIST).permitAll()
                .antMatchers("/api/registration").permitAll()
                .antMatchers("/api/registration/confirm-account").permitAll()
                .antMatchers("/api/login").permitAll()
                .antMatchers(HttpMethod.GET, "/test").authenticated()
                .antMatchers(HttpMethod.POST, "/api/habits").authenticated()
                .antMatchers(HttpMethod.GET, "/api/habits").authenticated()
                .antMatchers(HttpMethod.PUT, "/api/habits/{id}").authenticated()
                .antMatchers(HttpMethod.DELETE, "/api/habits/{id}").authenticated()
                .anyRequest().denyAll()
                .and()
                .addFilterBefore(new JwtTokenFilter(jwtTokenService), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(new JwtTokenFilterForBlackList(blackList, jwtTokenService), UsernamePasswordAuthenticationFilter.class)
                .logout()
                    .addLogoutHandler(new JwtTokenDoBlackListing(blackList, jwtTokenService, cookieService))
                    .logoutRequestMatcher(new AntPathRequestMatcher("/api/logout"))
                    .logoutSuccessHandler((new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK)));

    }
}