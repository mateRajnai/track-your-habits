package mate.rajnai.trackyourhabits.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import mate.rajnai.trackyourhabits.model.enumerate.TimeOfDay;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Habit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    private String generalDescription;

    @Column(nullable = false)
    private String goal;

    private String descriptionOfRegularity;

    @Column(nullable = false)
    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private Set<TimeOfDay> timesOfDay;

    @OneToMany(mappedBy = "habit")
    private List<Excuse> excuses;

    @Column(nullable = false)
    private String reward;

    @EqualsAndHashCode.Exclude
    private LocalDateTime startingDate;

    @EqualsAndHashCode.Exclude
    @JsonIgnore
    @ManyToOne
    private AppUser user;


    public void updateNameIfInputFieldNotNull(String name) {
        if (name != null)
            this.name = name;
    }

    public void updateGeneralDescriptionIfInputFieldNotNull(String generalDescription) {
        if (generalDescription != null)
            this.generalDescription = generalDescription;
    }

    public void updateDescriptionOfRegularityIfInputFieldNotNull(String descriptionOfRegularity) {
        if (descriptionOfRegularity != null)
            this.descriptionOfRegularity = descriptionOfRegularity;
    }

}
