package mate.rajnai.trackyourhabits.model.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Builder
public class AuthResponseDto {

    private final boolean success;
    private final String message;
    private final Map<String, Object> data = new HashMap<>();

    public void addData(String key, Object value) {
        this.data.put(key, value);
    }
}
