package mate.rajnai.trackyourhabits.model.dto;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Builder
@Getter
public class RegistrationDetailsDto {

    @NotBlank
    private final String username;

    @NotBlank
    private final String email;

    private final String firstName;

    @NotBlank
    private final String password;

}
