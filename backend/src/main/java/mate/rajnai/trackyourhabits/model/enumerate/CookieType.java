package mate.rajnai.trackyourhabits.model.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CookieType {

    JWT_HTTP_ONLY_NOT_SECURE("jwt", true, false, 36000, "/"),
    JWT_HTTP_ONLY_SECURE("jwt", true, true, 36000, "/");

    private final String name;
    private final boolean isHttpOnly;
    private final boolean isSecure;
    private final int maxAge;
    private final String path;

}
