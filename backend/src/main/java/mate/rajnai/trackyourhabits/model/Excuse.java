package mate.rajnai.trackyourhabits.model;

import javax.persistence.*;

@Entity
public class Excuse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Habit habit;
}
