package mate.rajnai.trackyourhabits.model.dto;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Builder
public class HabitDetailsDto {

    @NotNull
    private final String name;

    private final String generalDescription;

    @NotNull
    private final String goal;

    private final String descriptionOfRegularity;

    @NotNull
    private final List<String> timesOfDay;

    @NotNull
    private final String reward;

}
