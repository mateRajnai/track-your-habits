package mate.rajnai.trackyourhabits.model;

import lombok.*;
import mate.rajnai.trackyourhabits.model.enumerate.Role;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Builder
@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;

    private String firstName;

    @Column(unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private LocalDateTime registrationDate;

    @OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST)
    private Set<Habit> habits;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private List<Role> roles;

    @Setter
    private boolean isEnabled;

    public void addHabit(Habit habit) {
        if (habits == null) habits = new HashSet<>();
        this.habits.add(habit);
    }

    public void addRole(Role role) {
        roles.add(role);
    }

}
