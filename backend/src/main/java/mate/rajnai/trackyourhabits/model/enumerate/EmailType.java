package mate.rajnai.trackyourhabits.model.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmailType {

    AFTER_REGISTRATION("Confirmation email", "To confirm your account, please click here : ");

    private final String subject;
    private final String text;
}
