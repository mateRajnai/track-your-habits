package mate.rajnai.trackyourhabits.model.enumerate;

public enum TimeOfDay {
    MORNING, MIDDAY, AFTERNOON, EVENING
}
