package mate.rajnai.trackyourhabits.model.dto;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@Getter
public class LoginDetailsDto {

    @NotBlank
    private final String username;

    @NotBlank
    @NotNull
    private final String password;
}
