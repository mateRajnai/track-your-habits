package mate.rajnai.trackyourhabits.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ConfirmationTokenOfRegistration {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long tokenId;

    private final String confirmationToken = UUID.randomUUID().toString();;

    @Temporal(TemporalType.TIMESTAMP)
    private final Date createdDate = new Date();

    @OneToOne(targetEntity = AppUser.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private AppUser user;


}
