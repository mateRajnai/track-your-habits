package mate.rajnai.trackyourhabits.repository;

import mate.rajnai.trackyourhabits.model.AppUser;
import mate.rajnai.trackyourhabits.model.ConfirmationTokenOfRegistration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
@ActiveProfiles("test")
class ConfirmationTokenOfRegistrationRepositoryTest {


    private static final String ALREADY_REGISTERED_USERNAME = "username";
    private static final String ALREADY_REGISTERED_EMAIL = "email";
    private static final String PASSWORD = "pwd";

    @Autowired
    private ConfirmationTokenOfRegistrationRepository confirmationTokens;

    @Autowired
    private AppUserRepository users;

    private ConfirmationTokenOfRegistration confirmationToken;

    @BeforeEach
    public void saveUserAndToken() {
        AppUser appUser = AppUser.builder()
                .username(ALREADY_REGISTERED_USERNAME)
                .email(ALREADY_REGISTERED_EMAIL)
                .password(PASSWORD)
                .registrationDate(LocalDateTime.now())
                .build();
        users.saveAndFlush(appUser);
        confirmationToken = ConfirmationTokenOfRegistration.builder().user(appUser).build();
        confirmationTokens.saveAndFlush(confirmationToken);
    }

    @Test
    public void findSavedConfirmationToken() {
        String savedToken = confirmationToken.getConfirmationToken();
        assertThat(confirmationTokens.findByConfirmationToken(savedToken).orElse(null))
                .isEqualTo(confirmationToken);
    }

    @Test
    public void notFindNotSavedConfirmationToken() {
        String notSavedToken = "not saved token";
        assertThat(confirmationTokens.findByConfirmationToken(notSavedToken).orElse(null))
                .isEqualTo(null);
    }
}