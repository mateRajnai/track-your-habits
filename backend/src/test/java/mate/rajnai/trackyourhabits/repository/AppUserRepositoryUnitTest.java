package mate.rajnai.trackyourhabits.repository;

import mate.rajnai.trackyourhabits.model.AppUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
class AppUserRepositoryUnitTest {

    private static final String ALREADY_REGISTERED_USERNAME = "username";
    private static final String ALREADY_REGISTERED_EMAIL = "email";
    private static final String NOT_YET_REGISTERED_USERNAME = "username2";
    private static final String NOT_YET_REGISTERED_EMAIL = "email2";
    private static final String PASSWORD = "pwd";

    @Autowired
    private AppUserRepository users;

    private AppUser appUser;

    @BeforeEach
    public void init() {
        appUser = AppUser.builder()
                .username(ALREADY_REGISTERED_USERNAME)
                .email(ALREADY_REGISTERED_EMAIL)
                .password(PASSWORD)
                .registrationDate(LocalDateTime.now())
                .build();
        users.saveAndFlush(appUser);
    }

    @Test
    public void saveNewUser() {
        assertThat(users).isNotNull();
        assertThat(users.findAll()).hasSize(1);
    }

    @Test
    public void usernameIsOccupied() {
        assertThat(users
                .existsByUsername(ALREADY_REGISTERED_USERNAME)).isTrue();
    }

    @Test
    public void usernameIsNotOccupied() {
        assertThat(users
                .existsByUsername(NOT_YET_REGISTERED_USERNAME)).isFalse();
    }

    @Test
    public void emailIsOccupied() {
        assertThat((users
                .existsByEmail(ALREADY_REGISTERED_EMAIL))).isTrue();
    }

    @Test
    public void emailIsNotOccupied() {
        assertThat((users
                .existsByEmail(NOT_YET_REGISTERED_EMAIL))).isFalse();
    }

    @Test
    public void savedUserIsFoundByUsername() {
        assertThat(users
                .findByUsername(ALREADY_REGISTERED_USERNAME).orElse(null)).isEqualTo(appUser);
    }

    @Test
    public void notSavedUserIsNotFoundByUsername() {
        assertThat(users
                .findByUsername(NOT_YET_REGISTERED_USERNAME).orElse(null)).isEqualTo(null);
    }

}