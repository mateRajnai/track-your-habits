package mate.rajnai.trackyourhabits.repository;


import mate.rajnai.trackyourhabits.model.AppUser;
import mate.rajnai.trackyourhabits.model.Habit;
import mate.rajnai.trackyourhabits.model.enumerate.TimeOfDay;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
@ActiveProfiles("test")
class HabitRepositoryTest {

    @Autowired
    private AppUserRepository users;

    @Autowired
    private HabitRepository habits;

    private AppUser user;
    private Habit habit1;

    @BeforeEach
    public void init() {
        user = AppUser
                .builder()
                .username("username")
                .password("password")
                .email("email")
                .registrationDate(LocalDateTime.now())
                .build();
        habit1 = Habit.builder()
                .name("new habit")
                .goal("goal")
                .reward("reward")
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.MORNING)))
                .user(user)
                .build();
    }

    @Test
    public void findHabitsByUser() {
        Habit habit2 = Habit.builder()
                .name("new habit 2")
                .goal("goal")
                .reward("reward")
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.EVENING)))
                .user(user)
                .build();
        user.addHabit(habit1);
        user.addHabit(habit2);
        users.saveAndFlush(user);
        List<Habit> habits = this.habits.findByUser(user);
        assertThat(habits).hasSize(2);
    }

    @Test
    public void findHabitByIdAndUser() {
        user.addHabit(habit1);
        users.saveAndFlush(user);
        Long habitId = habit1.getId();
        assertThat(habits.findByIdAndUser(habitId, user).orElse(null))
                .isEqualTo(habit1);
    }

    @Test
    public void notFindingHabitByIdAndUser_wrongId() {
        user.addHabit(habit1);
        users.saveAndFlush(user);
        Long notExistingHabitId = habit1.getId() + 1;
        assertThat(habits.findByIdAndUser(notExistingHabitId, user).orElse(null))
                .isEqualTo(null);
    }

    @Test
    public void notFindingHabitByIdAndUser_wrongUser() {
        AppUser user2 = AppUser
                .builder()
                .username("username2")
                .password("password")
                .email("email2")
                .registrationDate(LocalDateTime.now())
                .build();
        user.addHabit(habit1);
        users.saveAndFlush(user);
        users.saveAndFlush(user2);
        Long notExistingHabitId = habit1.getId();
        assertThat(habits.findByIdAndUser(notExistingHabitId, user2).orElse(null))
                .isEqualTo(null);
    }

    @Test
    public void deleteHabitByIdAndUser_success_correctInputs() {
        users.saveAndFlush(user);
        habits.saveAndFlush(habit1);
        assertThat(habits.findAll()).hasSize(1);
        habits.deleteByIdAndUser(habit1.getId(), user);
        assertThat(habits.findAll()).hasSize(0);
    }

    @Test
    public void deleteHabitByIdAndUser_failure_wrongInputs() {
        AppUser userWhoDoesntHaveHabit = AppUser
                .builder()
                .username("username2")
                .password("password")
                .email("email2")
                .registrationDate(LocalDateTime.now())
                .build();
        users.saveAll(List.of(user, userWhoDoesntHaveHabit));
        habits.saveAndFlush(habit1);
        assertThat(habits.findAll()).hasSize(1);
        habits.deleteByIdAndUser(habit1.getId(), userWhoDoesntHaveHabit);
        assertThat(habits.findAll()).hasSize(1);
    }
}