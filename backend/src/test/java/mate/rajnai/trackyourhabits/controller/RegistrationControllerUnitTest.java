package mate.rajnai.trackyourhabits.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import mate.rajnai.trackyourhabits.model.dto.RegistrationDetailsDto;
import mate.rajnai.trackyourhabits.model.dto.AuthResponseDto;
import mate.rajnai.trackyourhabits.security.CookieService;
import mate.rajnai.trackyourhabits.security.jwttoken.JwtTokenBlackListDaoMem;
import mate.rajnai.trackyourhabits.security.jwttoken.JwtTokenService;
import mate.rajnai.trackyourhabits.service.RegistrationService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RegistrationController.class)
@ActiveProfiles("test")
class RegistrationControllerUnitTest {

    // This test class is unnecessary, because only the correct operation of Spring Mvc is tested here
    // But it was a good practise to learn how to test a controller

    private static final String URL_PREFIX_OF_REGISTER_API = "/api/registration";
    private static final String URL_PREFIX_OF_CONFIRM_ACCOUNT_API = "/api/registration/confirm-account";

    private static final String USERNAME_TO_BE_REGISTERED = "username";
    private static final String EMAIL_TO_BE_REGISTERED = "email";
    private static final String PASSWORD = "pwd";
    private static final String MESSAGE_WHEN_REGISTRATION_SUCCESSFUL = "Registration was successful!";
    private static final String CHARACTER_ENCODING_UTF_8 = "utf-8";
    private static final String MESSAGE_WHEN_CONFIRMATION_SUCCESSFUL = "Your email address is confirmed. Now you can log in.";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private RegistrationService registrationService;

    @MockBean
    private JwtTokenService jwtTokenService;

    @MockBean
    private JwtTokenBlackListDaoMem jwtTokenBlackListDaoMem;

    @MockBean
    private CookieService cookieService;

    @Test
    public void registerMethod_whenValidInput_thenReturns200() throws Exception {
        RegistrationDetailsDto user = RegistrationDetailsDto.builder()
                .username(USERNAME_TO_BE_REGISTERED)
                .email(EMAIL_TO_BE_REGISTERED)
                .password(PASSWORD)
                .build();

        String userAsJson = mapper.writeValueAsString(user);
        mockMvc.perform(post(URL_PREFIX_OF_REGISTER_API)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(userAsJson)
                    .characterEncoding(CHARACTER_ENCODING_UTF_8))
                .andExpect(status().isOk());
    }

    @Test
    public void registerMethod_whenInvalidInput_thenReturns400() throws Exception {
        RegistrationDetailsDto user = RegistrationDetailsDto.builder()
                .email(EMAIL_TO_BE_REGISTERED)
                .password(PASSWORD)
                .build();

        String userAsJson = mapper.writeValueAsString(user);
        mockMvc.perform(post(URL_PREFIX_OF_REGISTER_API)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(userAsJson)
                    .characterEncoding(CHARACTER_ENCODING_UTF_8))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void registerMethod_whenValidInput_thenRegisterMethodOfRegistrationServiceIsCalled() throws Exception {
        RegistrationDetailsDto user = RegistrationDetailsDto.builder()
                .username(USERNAME_TO_BE_REGISTERED)
                .email(EMAIL_TO_BE_REGISTERED)
                .password(PASSWORD)
                .build();

        String userAsJson = mapper.writeValueAsString(user);
        mockMvc.perform(post(URL_PREFIX_OF_REGISTER_API)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(userAsJson)
                    .characterEncoding(CHARACTER_ENCODING_UTF_8))
                .andExpect(status().isOk());

        verify(registrationService, times(1)).register(any(RegistrationDetailsDto.class));
    }

    // This test case is especially unnecessary, because only the correct operation of Spring Mvc is tested here
    @Test
    public void registerMethod_whenValidInput_thenReturnValueIsOk() throws Exception {
        RegistrationDetailsDto user = RegistrationDetailsDto.builder()
                .username(USERNAME_TO_BE_REGISTERED)
                .email(EMAIL_TO_BE_REGISTERED)
                .password(PASSWORD)
                .build();

        AuthResponseDto authResponseDto = AuthResponseDto
                .builder()
                .success(true)
                .message(MESSAGE_WHEN_REGISTRATION_SUCCESSFUL)
                .build();

        Mockito.when(registrationService.register(any(RegistrationDetailsDto.class))).thenReturn(authResponseDto);

        String userAsJson = mapper.writeValueAsString(user);
        MvcResult mvcResult = mockMvc.perform(post(URL_PREFIX_OF_REGISTER_API)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(userAsJson)
                    .characterEncoding(CHARACTER_ENCODING_UTF_8))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String expectedResponseBody = mapper.writeValueAsString(authResponseDto);
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void confirmAccountMethod_whenThereIsQueryParameter_thenReturns200() throws Exception {
        String queryParameter = "confirmation-token";
        mockMvc.perform(get(URL_PREFIX_OF_CONFIRM_ACCOUNT_API)
                    .param("token", queryParameter)
                    .characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @Test
    public void confirmAccountMethod_whenNoQueryParameter_thenReturns400() throws Exception {
        mockMvc.perform(get(URL_PREFIX_OF_CONFIRM_ACCOUNT_API)
                    .characterEncoding("utf-8"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void confirmAccountMethod_whenThereIsQueryParameter_thenConfirmAccountMethodOfRegistrationServiceIsCalled() throws Exception {
        String queryParameter = "confirmation-token";
        mockMvc.perform(get(URL_PREFIX_OF_CONFIRM_ACCOUNT_API)
                    .param("token", queryParameter)
                    .characterEncoding("utf-8"))
                .andExpect(status().isOk());

        verify(registrationService, times(1)).confirmAccount(queryParameter);
    }


    // This test case is especially unnecessary, because only the correct operation of Spring Mvc is tested here
    @Test
    public void confirmAccountMethod_whenThereIsQueryParameter_thenReturnValueIsOk() throws Exception {
        String queryParameter = "confirmation-token";
        AuthResponseDto authResponseDto = AuthResponseDto
                .builder()
                .success(true)
                .message(MESSAGE_WHEN_CONFIRMATION_SUCCESSFUL)
                .build();

        Mockito.when(registrationService.confirmAccount(queryParameter)).thenReturn(authResponseDto);
        MvcResult mvcResult = mockMvc.perform(get(URL_PREFIX_OF_CONFIRM_ACCOUNT_API)
                    .param("token", queryParameter)
                    .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andReturn();


        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String expectedResponseBody = mapper.writeValueAsString(authResponseDto);
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }
}
