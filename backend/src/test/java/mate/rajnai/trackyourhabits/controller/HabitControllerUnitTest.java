package mate.rajnai.trackyourhabits.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import mate.rajnai.trackyourhabits.model.Habit;
import mate.rajnai.trackyourhabits.model.enumerate.TimeOfDay;
import mate.rajnai.trackyourhabits.model.dto.HabitDetailsDto;
import mate.rajnai.trackyourhabits.security.CookieService;
import mate.rajnai.trackyourhabits.security.jwttoken.JwtTokenBlackListDaoMem;
import mate.rajnai.trackyourhabits.security.jwttoken.JwtTokenService;
import mate.rajnai.trackyourhabits.service.HabitService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(HabitController.class)
@ActiveProfiles("test")
class HabitControllerUnitTest {

    private static final String urlPrefix = "/api/habits";

    private static final String DTO_HABIT_NAME = "new habit";
    private static final String DTO_HABIT_GOAL = "goal";
    private static final String DTO_HABIT_REWARD = "reward";
    private static final ArrayList<String> DTO_HABIT_TIMES_OF_DAY = new ArrayList<>(Collections.singletonList("MORNING"));
    private static final Set<TimeOfDay> OUTPUT_HABIT_TIMES_OF_DAY = new HashSet<>(Collections.singletonList(TimeOfDay.MORNING));

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private HabitService habitService;

    @MockBean
    private JwtTokenService jwtTokenService;

    @MockBean
    private JwtTokenBlackListDaoMem jwtTokenBlackListDaoMem;

    @MockBean
    private CookieService cookieService;

    @Test
    public void addNewHabitMethod_whenValidInput_thenReturns200() throws Exception {
        HabitDetailsDto habitDetailsDto = HabitDetailsDto
                    .builder()
                    .name(DTO_HABIT_NAME)
                    .goal(DTO_HABIT_GOAL)
                    .reward(DTO_HABIT_REWARD)
                    .timesOfDay(DTO_HABIT_TIMES_OF_DAY)
                    .build();

        String habitAsJson = mapper.writeValueAsString(habitDetailsDto);
        mockMvc.perform(post(urlPrefix)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(habitAsJson))
                .andExpect(status().isCreated());
    }


    @Test
    public void addNewHabitMethod_whenInvalidInput_thenReturns400() throws Exception {
        HabitDetailsDto habitDetailsDto = HabitDetailsDto
                .builder()
                .timesOfDay(DTO_HABIT_TIMES_OF_DAY)
                .build();

        String habitAsJson = mapper.writeValueAsString(habitDetailsDto);
        mockMvc.perform(post(urlPrefix)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(habitAsJson))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void addNewHabitMethod_whenValidInput_thenAddNewHabitMethodOfHabitServiceIsCalled() throws Exception {
        HabitDetailsDto habitDetailsDto = HabitDetailsDto
                .builder()
                .name(DTO_HABIT_NAME)
                .goal(DTO_HABIT_GOAL)
                .reward(DTO_HABIT_REWARD)
                .timesOfDay(DTO_HABIT_TIMES_OF_DAY)
                .build();

        String habitAsJson = mapper.writeValueAsString(habitDetailsDto);
        mockMvc.perform(post(urlPrefix)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(habitAsJson))
                .andExpect(status().isCreated());

        verify(habitService, times(1)).addNewHabitToUser(any(HabitDetailsDto.class));
    }


    @Test
    public void addNewHabitMethod_whenValidInput_thenReturnsHabitInTheResponseBody() throws Exception {
        HabitDetailsDto habitDetailsDto = HabitDetailsDto
                .builder()
                .name(DTO_HABIT_NAME)
                .goal(DTO_HABIT_GOAL)
                .reward(DTO_HABIT_REWARD)
                .timesOfDay(DTO_HABIT_TIMES_OF_DAY)
                .build();

        Habit expectedHabit = Habit
                .builder()
                .id(1L)
                .name(DTO_HABIT_NAME)
                .timesOfDay(OUTPUT_HABIT_TIMES_OF_DAY)
                .build();

        Mockito.when(habitService.addNewHabitToUser(any(HabitDetailsDto.class)))
                .thenReturn(expectedHabit);

        String habitDetailsAsJson = mapper.writeValueAsString(habitDetailsDto);
        MvcResult mvcResult = mockMvc.perform(post(urlPrefix)
                .contentType(MediaType.APPLICATION_JSON)
                .content(habitDetailsAsJson)
                .characterEncoding("utf-8"))
                .andExpect(status().isCreated())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String expectedResponseBody = mapper.writeValueAsString(expectedHabit);
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }


    @Test
    public void listHabitsByUserMethod_getHabitsByUserMethodOfHabitServiceIsCalled_listHabitsByUserMethod() throws Exception {
        mockMvc.perform(get(urlPrefix))
                .andExpect(status().isOk());
        verify(habitService, times(1)).getHabitsByUser();
    }


    @Test
    public void gettingHabitsByUser_returnsHabitsInTheResponseBody() throws Exception {
        Habit habit1 = Habit.builder()
                .id(2L)
                .name("new habit")
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.MORNING)))
                .build();
        Habit habit2 = Habit.builder()
                .id(3L)
                .name("new habit 2")
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.EVENING)))
                .build();
        List<Habit> habits = new ArrayList<>(Arrays.asList(habit1, habit2));

        Mockito.when(habitService.getHabitsByUser())
                .thenReturn(habits);

        MvcResult mvcResult = mockMvc.perform(get(urlPrefix))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String expectedResponseBody = mapper.writeValueAsString(habits);
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }
}