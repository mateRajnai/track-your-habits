package mate.rajnai.trackyourhabits.security;

import mate.rajnai.trackyourhabits.service.EmailService;
import mate.rajnai.trackyourhabits.service.RegistrationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
class CookieServiceUnitTest {

    private static final String FIRST_COOKIE_NAME = "first-cookie";
    private static final String SECOND_COOKIE_NAME = "second-cookie";
    private static final String COOKIE_VALUE = "value";
    private static final boolean IS_HTTP_ONLY = true;
    private static final boolean IS_SECURE = true;
    private static final int MAX_AGE = 1000;
    private static final String PATH = "/";

    @Autowired
    private CookieService cookieService;

    @MockBean
    private RegistrationService registrationService;

    @MockBean
    private EmailService emailService;

    @MockBean
    private HttpServletRequest httpServletRequest;

    private Cookie firstCookie;

    @BeforeEach
    public void createACookie() {
        firstCookie = cookieService
                .createCookie(FIRST_COOKIE_NAME,
                        COOKIE_VALUE,
                        IS_HTTP_ONLY,
                        IS_SECURE,
                        MAX_AGE,
                        PATH);
    }

    @Test
    public void createdCookieHasCorrectProperties() {
        assertThat(firstCookie).extracting(Cookie::getName).isEqualTo(FIRST_COOKIE_NAME);
        assertThat(firstCookie).extracting(Cookie::getValue).isEqualTo(COOKIE_VALUE);
        assertThat(firstCookie).extracting(Cookie::isHttpOnly).isEqualTo(IS_HTTP_ONLY);
        assertThat(firstCookie).extracting(Cookie::getSecure).isEqualTo(IS_SECURE);
        assertThat(firstCookie).extracting(Cookie::getMaxAge).isEqualTo(MAX_AGE);
        assertThat(firstCookie).extracting(Cookie::getPath).isEqualTo(PATH);
    }

    @Test
    public void invalidatedCookieHasNullValueAndZeroMaxAgeAndHttpOnly() {
        Cookie secondCookie = cookieService
                .createCookie(SECOND_COOKIE_NAME,
                        COOKIE_VALUE,
                        IS_HTTP_ONLY,
                        IS_SECURE,
                        MAX_AGE,
                        PATH);
        Cookie[] cookies = new Cookie[2];
        cookies[0] = secondCookie;
        cookies[1] = firstCookie;
        Mockito.when(httpServletRequest.getCookies()).thenReturn(cookies);
        Cookie invalidatedCookie = cookieService.invalidateCookie(httpServletRequest, FIRST_COOKIE_NAME, true);
        assertThat(invalidatedCookie).extracting(Cookie::getName).isEqualTo(FIRST_COOKIE_NAME);
        assertThat(invalidatedCookie).extracting(Cookie::getValue).isEqualTo(null);
        assertThat(invalidatedCookie).extracting(Cookie::isHttpOnly).isEqualTo(IS_HTTP_ONLY);
        assertThat(invalidatedCookie).extracting(Cookie::getMaxAge).isEqualTo(0);
    }

}