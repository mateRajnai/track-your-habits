package mate.rajnai.trackyourhabits.service;

import mate.rajnai.trackyourhabits.exception.ConfirmationTokenNotFoundException;
import mate.rajnai.trackyourhabits.model.AppUser;
import mate.rajnai.trackyourhabits.model.dto.RegistrationDetailsDto;
import mate.rajnai.trackyourhabits.model.dto.AuthResponseDto;
import mate.rajnai.trackyourhabits.repository.AppUserRepository;
import mate.rajnai.trackyourhabits.repository.ConfirmationTokenOfRegistrationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
@ActiveProfiles("test")
class RegistrationServiceIntegrationTest {

    private static final String ALREADY_REGISTERED_USERNAME = "username";
    private static final String ALREADY_REGISTERED_EMAIL = "email";
    private static final String NOT_YET_REGISTERED_USERNAME = "username2";
    private static final String NOT_YET_REGISTERED_EMAIL = "email2";
    private static final String PASSWORD = "pwd";

    private static final String KEY_OF_USERNAME_IS_OCCUPIED = "usernameIsOccupied";
    private static final String KEY_OF_EMAIL_IS_OCCUPIED = "emailIsOccupied";

    @Autowired
    private AppUserRepository users;

    @Autowired
    private ConfirmationTokenOfRegistrationRepository confirmationTokens;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private EmailService emailService;

    private RegistrationService registrationService;

    @BeforeEach
    public void init() {
        registrationService = new RegistrationService(users, passwordEncoder, emailService, confirmationTokens);
        AppUser registeredUser = AppUser.builder()
                .username(ALREADY_REGISTERED_USERNAME)
                .email(ALREADY_REGISTERED_EMAIL)
                .password(Mockito
                        .when(passwordEncoder.encode(PASSWORD))
                        .thenReturn("hashcode")
                        .toString())
                .registrationDate(LocalDateTime.now())
                .build();
        users.saveAndFlush(registeredUser);
    }

    @Test
    public void registrationFails_UsernameAlreadyOccupied() {
        RegistrationDetailsDto newUser = RegistrationDetailsDto.builder()
                .username(ALREADY_REGISTERED_USERNAME)
                .email(NOT_YET_REGISTERED_EMAIL)
                .password(PASSWORD)
                .build();
        Mockito.when(passwordEncoder.encode(PASSWORD)).thenReturn("hashcode");
        AuthResponseDto authResponseDto = registrationService.register(newUser);
        assertThat(authResponseDto.getData().get(KEY_OF_USERNAME_IS_OCCUPIED)).isEqualTo(true);
    }

    @Test
    public void registrationFails_BecauseEmailAlreadyOccupied() {
        RegistrationDetailsDto newUser = RegistrationDetailsDto.builder()
                .username(NOT_YET_REGISTERED_USERNAME)
                .email(ALREADY_REGISTERED_EMAIL)
                .password(PASSWORD)
                .build();
        Mockito.when(passwordEncoder.encode(PASSWORD)).thenReturn("hashcode");
        AuthResponseDto authResponseDto = registrationService.register(newUser);
        assertThat(authResponseDto.getData().get(KEY_OF_EMAIL_IS_OCCUPIED)).isEqualTo(true);
    }

    @Test
    public void registrationSuccessful() {
        RegistrationDetailsDto appUser = RegistrationDetailsDto.builder()
                .username(NOT_YET_REGISTERED_USERNAME)
                .email(NOT_YET_REGISTERED_EMAIL)
                .password(PASSWORD)
                .build();
        Mockito.when(passwordEncoder.encode(PASSWORD)).thenReturn("hashcode");
        Mockito.doNothing().when(emailService).sendEmail(NOT_YET_REGISTERED_EMAIL, "subject", "text");
        AuthResponseDto authResponseDto = registrationService.register(appUser);
        assertThat(authResponseDto.isSuccess()).isTrue();
    }

    @Test
    public void confirmationTokenOfUserIsSavedAfterSuccessfulRegistrationAndBeforeEmailConfirmation() {
        RegistrationDetailsDto appUser = RegistrationDetailsDto.builder()
                .username(NOT_YET_REGISTERED_USERNAME)
                .email(NOT_YET_REGISTERED_EMAIL)
                .password(PASSWORD)
                .build();
        Mockito.doNothing().when(emailService).sendEmail(appUser.getEmail(), "subject", "text");
        AuthResponseDto authResponseDto = registrationService.register(appUser);
        assertThat(authResponseDto.isSuccess()).isTrue();
        AppUser registeredUser = users.findByUsername(appUser.getUsername()).orElse(null);
        assertThat(confirmationTokens.findByUser(registeredUser)).isNotNull();
    }

    @Test
    public void userIsNotEnabledAfterSuccessfulRegistrationAndBeforeEmailConfirmation() {
        RegistrationDetailsDto appUser = RegistrationDetailsDto.builder()
                .username(NOT_YET_REGISTERED_USERNAME)
                .email(NOT_YET_REGISTERED_EMAIL)
                .password(PASSWORD)
                .build();
        Mockito.doNothing().when(emailService).sendEmail(appUser.getEmail(), "subject", "text");
        AuthResponseDto authResponseDto = registrationService.register(appUser);
        AppUser nowRegisteredUser = users.findByUsername(appUser.getUsername()).orElse(null);
        assertThat(authResponseDto.isSuccess()).isTrue();
        assert nowRegisteredUser != null;
        assertThat(nowRegisteredUser.isEnabled()).isFalse();
    }

    @Test
    public void userIsEnabledAfterSuccessfulRegistrationAndEmailConfirmation() {
        RegistrationDetailsDto appUser = RegistrationDetailsDto.builder()
                .username(NOT_YET_REGISTERED_USERNAME)
                .email(NOT_YET_REGISTERED_EMAIL)
                .password(PASSWORD)
                .build();
        Mockito.doNothing().when(emailService).sendEmail(appUser.getEmail(), "subject", "text");
        AuthResponseDto authResponseDto = registrationService.register(appUser);
        AppUser nowRegisteredUser = users.findByUsername(appUser.getUsername()).orElse(null);
        String confirmationToken = confirmationTokens.findByUser(nowRegisteredUser).getConfirmationToken();
        registrationService.confirmAccount(confirmationToken);
        assertThat(authResponseDto.isSuccess()).isTrue();
        assert nowRegisteredUser != null;
        assertThat(nowRegisteredUser.isEnabled()).isTrue();
    }

    @Test
    public void confirmationTokenOfUserIsDeletedAfterSuccessfulRegistrationAndEmailConfirmation() {
        RegistrationDetailsDto appUser = RegistrationDetailsDto.builder()
                .username(NOT_YET_REGISTERED_USERNAME)
                .email(NOT_YET_REGISTERED_EMAIL)
                .password(PASSWORD)
                .build();
        Mockito.doNothing().when(emailService).sendEmail(appUser.getEmail(), "subject", "text");
        AuthResponseDto authResponseDto = registrationService.register(appUser);
        assertThat(authResponseDto.isSuccess()).isTrue();
        AppUser nowRegisteredUser = users.findByUsername(appUser.getUsername()).orElse(null);
        String confirmationToken = confirmationTokens.findByUser(nowRegisteredUser).getConfirmationToken();
        registrationService.confirmAccount(confirmationToken);
        assertThat(confirmationTokens.findByConfirmationToken(confirmationToken).orElse(null)).isNull();
    }

    @Test
    public void whileConfirmingAccountConfirmationTokenIsNotFound_throwsException() {
        RegistrationDetailsDto appUser = RegistrationDetailsDto.builder()
                .username(NOT_YET_REGISTERED_USERNAME)
                .email(NOT_YET_REGISTERED_EMAIL)
                .password(PASSWORD)
                .build();
        Mockito.doNothing().when(emailService).sendEmail(appUser.getEmail(), "subject", "text");
        AuthResponseDto authResponseDto = registrationService.register(appUser);
        assertThat(authResponseDto.isSuccess()).isTrue();
        String wrongConfirmationToken = "wrong-confirmation-token";
        assertThrows(ConfirmationTokenNotFoundException.class, () -> registrationService.confirmAccount(wrongConfirmationToken));
    }
}
