package mate.rajnai.trackyourhabits.service;

import mate.rajnai.trackyourhabits.model.dto.AuthResponseDto;
import mate.rajnai.trackyourhabits.model.dto.RegistrationDetailsDto;
import mate.rajnai.trackyourhabits.repository.AppUserRepository;
import mate.rajnai.trackyourhabits.repository.ConfirmationTokenOfRegistrationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
class RegistrationServiceUnitTest {

    private static final String KEY_OF_USERNAME_IS_OCCUPIED = "usernameIsOccupied";
    private static final String KEY_OF_EMAIL_IS_OCCUPIED = "emailIsOccupied";

    @Mock
    private AppUserRepository users;

    @Mock
    private ConfirmationTokenOfRegistrationRepository confirmationTokens;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private EmailService emailService;

    @InjectMocks
    private RegistrationService registrationService;

    private RegistrationDetailsDto appUser;

    @BeforeEach
    public void init() {
        appUser = RegistrationDetailsDto.builder()
                .username("username")
                .email("email")
                .password("pwd")
                .build();
        Mockito.when(passwordEncoder.encode("pwd")).thenReturn("hashcode");
    }

    @Test
    public void registrationFails_UsernameAlreadyOccupied() {
        Mockito.when(users.existsByUsername("username")).thenReturn(true);
        Mockito.when(users.existsByEmail("email")).thenReturn(false);
        AuthResponseDto authResponseDto = registrationService.register(appUser);
        assertThat(authResponseDto.isSuccess()).isFalse();
        assertThat(authResponseDto.getData().get(KEY_OF_USERNAME_IS_OCCUPIED)).isEqualTo(true);
    }


    @Test
    public void registrationFails_EmailAlreadyOccupied() {
        Mockito.when(users.existsByUsername("username")).thenReturn(false);
        Mockito.when(users.existsByEmail("email")).thenReturn(true);
        AuthResponseDto authResponseDto = registrationService.register(appUser);
        assertThat(authResponseDto.isSuccess()).isFalse();
        assertThat(authResponseDto.getData().get(KEY_OF_EMAIL_IS_OCCUPIED)).isEqualTo(true);
    }

    @Test
    public void registrationSuccessful() {
        Mockito.when(users.existsByUsername("username")).thenReturn(false);
        Mockito.when(users.existsByEmail("email")).thenReturn(false);
        Mockito.doNothing().when(emailService).sendEmail(appUser.getEmail(), "subject", "text");
        AuthResponseDto authResponseDto = registrationService.register(appUser);
        assertThat(authResponseDto.isSuccess()).isTrue();
        assertThat(authResponseDto.getData()).isEqualTo(new HashMap<>());
    }
}
