package mate.rajnai.trackyourhabits.service;

import mate.rajnai.trackyourhabits.exception.HabitNotFoundException;
import mate.rajnai.trackyourhabits.exception.UserNotFoundException;
import mate.rajnai.trackyourhabits.model.AppUser;
import mate.rajnai.trackyourhabits.model.Habit;
import mate.rajnai.trackyourhabits.model.enumerate.TimeOfDay;
import mate.rajnai.trackyourhabits.model.dto.HabitDetailsDto;
import mate.rajnai.trackyourhabits.repository.AppUserRepository;
import mate.rajnai.trackyourhabits.repository.HabitRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.*;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class HabitServiceIntegrationTest {

    private static final String MESSAGE_WHEN_USER_NOT_FOUND = "User not found";

    @Autowired
    private AppUserRepository users;

    @Autowired
    private HabitRepository habits;

    @MockBean
    private UserInfoService userInfoService;

    private HabitService habitService;

    private AppUser user;


    @BeforeEach
    public void init() {
        habitService = new HabitService(users, habits, userInfoService);
        user = AppUser.builder()
                .username("name")
                .password("pwd")
                .email("email")
                .registrationDate(LocalDateTime.now())
                .build();
        users.saveAndFlush(user);
    }

    @Test
    public void testAddingNewHabit() {
        HabitDetailsDto habitDetailsDto = HabitDetailsDto
                .builder()
                .name("new habit")
                .reward("reward")
                .goal("goal")
                .timesOfDay(new ArrayList<>(Arrays.asList("MORNING")))
                .build();
        Habit expected = Habit.builder()
                .id(1L)
                .name("new habit")
                .reward("reward")
                .goal("goal")
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.MORNING)))
                .user(user)
                .build();
        Mockito.when(userInfoService.getNameOfCurrentUser()).thenReturn(user.getUsername());
        assertThat(habitService.addNewHabitToUser(habitDetailsDto)).isEqualTo(expected);
    }


    @Test
    public void testOfThrowingExceptionWhenAddingNewHabit_userNotExists() {
        HabitDetailsDto habitDetailsDto = HabitDetailsDto
                .builder()
                .name("new habit")
                .timesOfDay(new ArrayList<>(Arrays.asList("MORNING")))
                .build();
        Mockito.when(userInfoService.getNameOfCurrentUser()).thenThrow(new UserNotFoundException(MESSAGE_WHEN_USER_NOT_FOUND));
        assertThrows(UserNotFoundException.class, () -> habitService.addNewHabitToUser(habitDetailsDto));
    }

    @Test
    public void testFindingHabitsByUser() {
        Habit habit1 = Habit.builder()
                .name("new habit")
                .reward("reward")
                .goal("goal")
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.MORNING)))
                .user(user)
                .build();

        Habit habit2 = Habit.builder()
                .name("new habit 2")
                .reward("reward")
                .goal("goal")
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.EVENING)))
                .user(user)
                .build();
        this.habits.saveAndFlush(habit1);
        this.habits.saveAndFlush(habit2);
        List<Habit> habitsOfUser = habits.findByUser(user);
        assertThat(habitsOfUser).hasSize(2);
        assertThat(habitsOfUser).containsExactly(habit1, habit2);
    }

    @Test
    public void testOfThrowingExceptionWhenGettingHabitsByUser_userNotExists() {
        Mockito.when(userInfoService.getNameOfCurrentUser()).thenThrow(new UserNotFoundException(MESSAGE_WHEN_USER_NOT_FOUND));
        assertThrows(UserNotFoundException.class, () -> habitService.getHabitsByUser());
    }

    @Test
    public void testOfUpdatingExistingHabit() {
        Habit habit1 = Habit.builder()
            .id(1L)
            .name("new habit")
            .reward("reward")
            .goal("goal")
            .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.MORNING)))
            .user(user)
            .build();
        this.habits.saveAndFlush(habit1);
        HabitDetailsDto habitDetailsDto = HabitDetailsDto.builder()
                .name("modified habit")
                .reward("reward")
                .goal("goal")
                .timesOfDay(List.of("MORNING"))
                .build();
        Habit updatedHabit = Habit.builder()
                .id(1L)
                .name(habitDetailsDto.getName())
                .reward(habitDetailsDto.getReward())
                .goal(habitDetailsDto.getGoal())
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.MORNING)))
                .user(user)
                .build();

        Mockito.when(this.userInfoService.getNameOfCurrentUser()).thenReturn(user.getUsername());
        assertThat(this.habitService.updateHabit(habitDetailsDto, habit1.getId()))
                .isEqualTo(updatedHabit);
    }

    @Test
    public void testOfThrowingExceptionWhenUpdatingHabit_habitNotExists() {
        HabitDetailsDto habitDetailsDto = HabitDetailsDto.builder()
                .name("modified habit")
                .reward("reward")
                .goal("goal")
                .timesOfDay(List.of("MORNING"))
                .build();
        Long notExistingHabitId = 2L;
        Mockito.when(this.userInfoService.getNameOfCurrentUser()).thenReturn(user.getUsername());
        assertThrows(HabitNotFoundException.class,
                () -> this.habitService.updateHabit(habitDetailsDto, notExistingHabitId));
    }

    @Test
    public void testOfDeletingExistingHabit() {
        Habit habit = Habit.builder()
                .name("new habit")
                .reward("reward")
                .goal("goal")
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.MORNING)))
                .user(user)
                .build();
        this.habits.saveAndFlush(habit);
        Mockito.when(this.userInfoService.getNameOfCurrentUser()).thenReturn(user.getUsername());
        assertThat(this.habits.findAll()).hasSize(1);
        this.habits.deleteByIdAndUser(habit.getId(), user);
        assertThat(this.habits.findAll()).hasSize(0);
    }

    @Test
    public void testOfNotDeletingExistingHabit_wrongUser() {
        Habit habit = Habit.builder()
                .name("new habit")
                .reward("reward")
                .goal("goal")
                .timesOfDay(new HashSet<>(Collections.singletonList(TimeOfDay.MORNING)))
                .user(user)
                .build();
        AppUser userNotHavingHabit = AppUser.builder()
                .username("name2")
                .password("pwd")
                .email("email2")
                .registrationDate(LocalDateTime.now())
                .build();
        this.users.saveAndFlush(userNotHavingHabit);
        this.habits.saveAndFlush(habit);
        Mockito.when(this.userInfoService.getNameOfCurrentUser()).thenReturn(user.getUsername());
        assertThat(this.habits.findAll()).hasSize(1);
        this.habits.deleteByIdAndUser(habit.getId(), userNotHavingHabit);
        assertThat(this.habits.findAll()).hasSize(1);
    }
}